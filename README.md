# TransBoot

TransBoot is three things: 

2. A collection of programs to facilitate booting form the internet.
1. An agent to configure your machine to boot from the internet. (And create bootable images from existing systems.)
3. A hosting service for bootable images.

TransBoot's target audience are day-to-day Linux users, who may not have the technical know how to create a recovery disk (USB, CD, etc.) and reconfigure their desktop. TransBoot will allow users to upload private pre-configured recovery images that can be deployed onto Linux machines via the internet.

Features

- Simple installation
- No need for physical installation medium   
- WiFi Support

Tech-Features

- Per device/user access control
- Self hosting
- Free software (GPLv3) and hackable
- Industry standard (iPXE)

# TransBoot

- Maintain a striped down fork of IPXE. 
- Hardcode to use HTTPS and TransBoot only.

Build the ipxe iso.

	git clone git@gitlab:Pmaynard/ipxe.git 
	cd src
	make bin/ipxe.iso -j EMBED=../../transboot/transboot/auto.ipxe
	file bin/ipxe.iso

# TansBoot Agent

- Add a GRUB entry to initiate TransBoot.
- Place the ISO in the appropriate location which GRUB can access.

	mke2fs /dev/mmcblk0p1 
	mount -t ext2 /dev/mmcblk0p1 /mnt
	mkdir /mnt/boot
	grub-install --boot-directory=/mnt/boot /dev/mmcblk0p1
	cp ../ipxe/src/bin/ipxe.iso /mnt/transboot.iso

<https://www.gnu.org/software/grub/manual/grub/grub.html#Loopback-booting>

	menuentry "TransBoot - Recovery" {
		set isofile="/transboot.iso"
		loopback loop (hd0,5)$isofile
		linux16 (loop)/ipxe.krn
	}

# TransBoot Service

- Provide access to boot images.
- Allow users to register and upload custom images.
- Authenticate users. 
- Map users' machines (MAC) to specific images. 

	cd service/www
	python -m SimpleHTTPServer 8989


